<?php
   get_header();
?>

<!--Blog Page-->
<?php
   $hero = get_field('hero');
   $content = get_field('page_content');
   $image = $hero['hero_image'];
   ?>
<div id="top-wrapper">
   <section id="sHero">
      <div class="heroContainer">
         <div class="heroImageContainer imageOverlay">
            <span><img alt="" src="<?php bloginfo('template_directory')?>/images/friendsplayingcardsSW.jpg"></span>
         </div>
         <div class="secondaryHeroDescription">
            <h1>Our Blog</h1>
         </div>
      </div>
   </section>
</div>
<div id="Wrapper">
   <div id="body-wrapper">
      <div class="wrapper_content">
         <div class="container contentSection">
            <div class="row">
               <div class="col-sm-3 blogControls">
                  <div id="left_content">
                     <div>
                        <?php if(is_active_sidebar('blog-sidebar')):?>
                        <?php dynamic_sidebar('blog-sidebar')?>
                        <?php endif;?>
                     </div>
                  </div>
               </div>
               <div class="col-sm-9">
                  <div id="right_content">
                     <?php get_template_part('includes/section', 'content');?>
                     <?php
                        global $wp_query;
                        $big = 99999999999;
                        
                        echo paginate_links( array(
                            'base' => str_replace($big,'%#%', esc_url(get_pagenum_link($big))),
                            'format' => '?paged=%#%',
                            'current' => max(1, get_query_var('paged')),
                            'total' => $wp_query->max_num_pages
                        ));
                        ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php get_footer(); ?>