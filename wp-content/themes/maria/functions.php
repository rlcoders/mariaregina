<?php

function load_stylesheets()
{
    wp_register_style('menu-red', get_template_directory_uri() . '/css/Menu.Red.css?637393811900045353', array(), 1, 'all');
    wp_enqueue_style('menu-red');

    wp_register_style('client', get_template_directory_uri() . '/css/Client.css?637393811900045353', array(), 1, 'all');
    wp_enqueue_style('client');

    wp_register_style('responsiveslides', get_template_directory_uri() . '/css/responsiveslides.css', array(), 1, 'all');
    wp_enqueue_style('responsiveslides');

    wp_register_style('magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(), 1, 'all');
    wp_enqueue_style('magnific-popup');

    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), 1, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('Control', get_template_directory_uri() . '/css/Control.css', array(), 1, 'all');
    wp_enqueue_style('Control');

    wp_register_style('Design', get_template_directory_uri() . '/css/Design.css', array(), 1, 'all');
    wp_enqueue_style('Design');

    wp_register_style('BlogStyle', get_template_directory_uri() . '/css/e74Blog.css', array(), 1, 'all');
    wp_enqueue_style('BlogStyle');
}

function load_js()
{
    wp_register_script('magnific', get_template_directory_uri() . '/js/magnific.124578.min.js', array(), 1, 1, 1);
    wp_enqueue_script('magnific');

    wp_register_script('responsiveslides', get_template_directory_uri() . '/js/responsiveslides.min.js', array(), 1, 1, 1);
    wp_enqueue_script('responsiveslides');

    wp_register_script('design', get_template_directory_uri() . '/js/Design.js', '', 1, 1, 1, 1);
    wp_enqueue_script('design');

}

add_action('wp_enqueue_scripts', 'load_stylesheets');
add_action('wp_enqueue_scripts', 'load_js');

//menu support
add_theme_support('menus');
add_theme_support('widgets');
add_theme_support('title-tag');

/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
add_theme_support('post-thumbnails');



/*
	 * Enable support for Post Formats.
	 *
	 * See: https://wordpress.org/support/article/post-formats/
	 */
add_theme_support(
    'post-formats',
    array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    )
);

//Register menu
register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'maria')
    )
);

function theme_siderbars(){
    register_sidebar(
        array(
            'name' => 'Blog Sidebar',
            'id' => 'blog-sidebar',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>'
        )
    );
}
add_action('widgets_init','theme_siderbars');

if(function_exists('acf_add_options_page')){
    acf_add_options_page(
		array(
			'page_title' => 'Website Settings',
			'menu_title' => 'Website Settings',
			'menu_slug' => 'website-settings',
			'capability' => 'edit_posts',
			'icon_url' => 'dashicons-admin-tools'			
		)
	);
	
	acf_add_options_sub_page(
		array(
			'page_title' => 'Header Settings',
			'menu_title' => 'Header',
			'parent_slug' => 'website-settings'		
		)
	);
	
	acf_add_options_sub_page(
		array(
			'page_title' => 'Footer Settings',
			'menu_title' => 'Footer',
			'parent_slug' => 'website-settings'		
		)
	);
	
		acf_add_options_sub_page(
		array(
			'page_title' => 'Notification Settings',
			'menu_title' => 'Popup Notification',
			'parent_slug' => 'website-settings'		
		)
	);
}

function contact_shortcode( $attr ) {
    ob_start();
    get_template_part('includes/section', 'contact');
    return ob_get_clean();
}
add_shortcode( 'contact', 'contact_shortcode' );


function custom_excerpt_length( $length ) {
    return 35;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
    return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');