<?php
/* Template Name: Post */
get_header();

?>

<?php
$hero = get_field('hero');
$content = get_field('page_content');
$image = $hero['hero_image'];
?>

<?php if ($image) : ?>
<section id="sHero">
    <div class="heroContainer">
        <div class="heroImageContainer imageOverlay">
            <span><img alt="" src="<?php echo $image  ?>" /></span>
        </div>
        <div class="secondaryHeroDescription">
            <span>
                <h1><?php echo $hero['main_title']; ?></h1>
            </span>
        </div>

    </div>
</section>
<?php endif;  ?>


<div class="container contentSection" id="secondaryContainer">
    <div class="com-md-12">
        <div class="sHeading">
            <div id="secondaryPage-Header">
                <?php echo $content ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>