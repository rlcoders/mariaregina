<?php get_header(); ?>
<?php
$hero = get_field('hero');
$notification = get_field('notification_text', 'option');
?>
<?php if (!empty($notification)) : ?>
   <div id="myModal" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
               <?php the_field('notification_text', 'option'); ?>
            </div>
         </div>
      </div>
   </div>
<?php endif; ?>
<!-- Home Rotator -->
<main id="main-content">
   <section class="video_hero" style="position: relative;">
      <div class="vimeo-wrapper">
         <iframe src="<?php echo $hero['videourl']; ?>?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
     <!-- <div class="heroDescription">
             <h1><span class="accentText"><?php echo $hero['main_title']; ?><br />
                  </span><?php echo $hero['sub_title']; ?>
               </h1>
               <a href=""></a>
      </div>-->
   </section>
   <?php
   $introduction = get_field('introduction');
   $intro = $introduction['intro'];
   $image = $introduction['intro_image'];
   $service = get_field('care_services');
   $services = $service['services'];
   $s1 = $services['service_1'];
   $s2 = $services['service_2'];
   $s3 = $services['service_3'];
   $blog = get_field('blog_posts');
   $posts = $blog['posts'];
   $gallery = get_field('image_gallery');
   $galleryimages = $gallery['images'];
   // Image variables.
   $url = $image['url'];
   $alt = $image['alt'];
   ?>

<section id="intro-text-forms">
   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <?php echo get_field('intro_section')['intro_text']; ?>
         </div>
         <div class="col-md-6">
            <div class="home-contact">
               <?php get_template_part('includes/section', 'contact');?>
            </div>
         </div>
      </div>
   </div>

</section>
<!--    <section id="excellenceInNursing">
      <div class="container noPadding containerFix">
         <div class="col-md-5 noPadding notRespImage">
            <span><img alt="<?php echo $alt  ?>" src="<?php echo $url  ?>" /></span>
         </div>
         <div class="col-md-7 excellenceSection">
            <div class="excellenceText">
               <div class="excellenceLogo">
                  <img alt="" src="<?php bloginfo('template_directory') ?>/images/openHands.png" />
               </div>
               <h2><?php echo $intro['intro_title']; ?></h2>
               <p><?php echo $intro['intro_text']; ?></p>
            </div>
         </div>
      </div>
   </section> -->
   <section id="best">
      <div class="altBackground">
         <div class="container bestContainer">
            <div class="bc_wrp">
               <div class="bc-img-wrap">
                  <?php
                     $best_img = get_field('best_image');
                     if($best_img != ''){
                        echo '<img src="'.$best_img['url'].'" '.$best_img['alt'].'/>';
                     }
                  ?>
               </div>
               <div class="bc-cont-wrap">
                  <?php echo get_field('best_content'); ?>
               </div>
            </div>



            <!--<div class="row justify-content-center">
              <div class="col-md-4 order-last">
                  <?php
                     $best_img = get_field('best_image');
                     if($best_img != ''){
                        echo '<img src="'.$best_img['url'].'" '.$best_img['alt'].'/>';
                     }
                  ?>
               </div>
               <div class="col-md-8">
                  <?php echo get_field('best_content'); ?>
               </div>
            </div>-->



            <!--<div class="bestContainerFix">
               <div class="bestContainerTable">
                  <div class="bestContainerContent">
                     <span>
                        <table style="width: 100%;">
                           <tbody>
                              <tr>
                                 <td style="text-align: center;">
                                    <?php
                                    $link = $introduction['link'];
                                    if ($link) : ?>
                                       <a href="<?php echo $link; ?>" tabindex="-1">
                                          <span style="font-size: 24px;"><?php echo $introduction['link_text'] ?></span></a>
                                    <?php endif; ?>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </span>
                  </div>
               </div>
            </div>-->

         </div>
      </div>
   </section>

   <section id="careServices">
      <div class="careServicesContainer">
         <h2><?php echo $service['services_title']; ?></h2>
         <div class="container carePods">
            <div class="row">
               <div class="col-md-4 carePod noPadding">
                  <?php if ($s1['service_1_link']) : ?>
                     <span><img alt="" src="<?php echo $s1['service_1_image']['url'] ?>" /></span>
                     <a href="<?php echo $s1['service_1_link']['url']; ?>">
                        <div class="carePodText">
                           <p><?php echo $s1['service_1_title']; ?></p>
                        </div>
                     </a>
                  <?php endif; ?>
               </div>
               <div class="col-md-4 carePod noPadding">
                  <?php
                  if ($s2['service_2_link']) : ?>
                     <span><img alt="" src="<?php echo $s2['service_2_image']['url'] ?>" /></span>
                     <a href="<?php echo $s2['service_2_link']['url']; ?>">
                        <div class="carePodText">
                           <p><?php echo $s2['service_2_title']; ?></p>
                        </div>
                     </a>
                  <?php endif; ?>
               </div>
               <div class="col-md-4 carePod noPadding">
                  <?php
                  if ($s3['service_3_link']) : ?>
                     <span><img alt="" src="<?php echo $s3['service_3_image']['url'] ?>" /></span>
                     <a href="<?php echo $s3['service_3_link']['url']; ?>">
                        <div class="carePodText">
                           <p><?php echo $s3['service_3_title']; ?></p>
                        </div>
                     </a>
                  <?php endif; ?>
               </div>
            </div>
         </div>
      </div>
      </div>
   </section>
   <section id="newsAndEvents">
      <div class="newsContainer">
         <h2><em><?php echo get_field('blog_posts')['title']; ?></em></h2>
         <div class="container newsPods">
            <div id="newsCarousel" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner" role="listbox">
                  <?php foreach ($posts as $post) : // variable must be called $post (IMPORTANT)
                  ?>
                     <?php setup_postdata($post); ?>
                     <div class="col-md-4 noPadding newsPod item">
                        <div class="newsPodText">
                           <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                           <span><?php the_excerpt(); ?></span>
                           <span><a href="<?php the_permalink(); ?>">Read More</a></span>
                        </div>
                     </div>
                  <?php endforeach; ?>
               </div>
            </div>
         </div>
         <?php if ($blog['link']) : ?>
            <div class="container">
               <div class="row">
                  <div class="col-md-4 col-md-offset-4 buttonContainer noPadding">
                     <a id="newsButton" class="buttonStyle" href="<?php echo $blog['link']; ?>"><?php echo $blog['link_text']; ?></a>
                  </div>
               </div>
            </div>
         <?php endif; ?>
      </div>
   </section>
   <section id="community">
      <div class="altBackground padCommunity">
         <div class="communityContainer">
            <h2><?php echo $gallery['title'] ?></h2>
<!--            <p>
               <?php echo $gallery['description'] ?>
            </p> -->
            <div class="container">
               <div class="row">
                  <div class="communityImages">
                     <div class="col-md-12 noPadding communityImage">
                        <div class="vimeo">
                            <!--<iframe src="<?php echo $galleryimages['image_1']; ?>?autoplay=1&controls=0&showinfo=0&autohide=1&loop=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
                            <div class="vid_ovly_wrap">
                              <div class="vid_ovly"></div>
                              <div class="play-right"></div>
                              <video controls id="gal_vid">
                                <source src="<?php echo get_stylesheet_directory_uri() ?>/images/Video_Vignettes_4.mp4" type="video/mp4">
                                Your browser does not support HTML video.
                              </video>
                            </div>
                           <script>
                              $('.vid_ovly').click(function(){
                                $('#gal_vid').get(0).play();
                                 $(this).remove();
                                 $('.play-right').remove();
                                 //this.paused?this.play():this.pause();
                              });
                              $('.play-right').click(function(){
                                $('#gal_vid').get(0).play();
                                 $(this).remove();
                                  $('.vid_ovly').remove();
                                 //this.paused?this.play():this.pause();
                              });
                           </script>
                           <style>
                              .play-right {
                                  width: 0;
                                  height: 0;
                                  border-top: 34px solid transparent;
                                  border-bottom: 34px solid transparent;
                                  border-left: 60px solid #fff;
                                  z-index: 999;
                                  position: absolute;
                                  -webkit-transform: translate(-50%,-50%);
                                  -ms-transform: translate(-50%,-50%);
                                  transform: translate(-50%,-50%);
                                  top: 50%;
                                  left: 50%;

                              }
                              @media(max-width: 550px){
                                 .play-right {
                                     border-top: 14px solid transparent;
                                     border-bottom: 14px solid transparent;
                                     border-left: 26px solid #fff;
                                  }
                              }
                           </style>
                        </div>
<!--                         <div class="col-md-7 noPadding communityImage communitySubimage">
                           <img src="<?php echo $galleryimages['image_3'] ?>" />
                        </div>
                        <div class="col-md-5 noPadding communityImage communitySubimage">
                           <img src="<?php echo $galleryimages['image_4'] ?>" />
                        </div> -->
                     </div>
<!--                      <div class="col-md-4 noPadding communityImage">
                        <img src="<?php echo $galleryimages['image_2'] ?>" />
                        <img class="gallery-img-5" src="<?php echo $galleryimages['image_5'] ?>" />
                     </div> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>

   <script>
      $(document).ready(function() {
         $("#myModal").modal('show');
      });
   </script>
   <style>
      .modal-content img {
         max-width: 95%;
      }
   </style>
   <?php get_footer(); ?>
