<?php
/* Template Name: Contact */
get_header();

?>


<?php
$hero = get_field('hero');
$content = get_field('page_content');
$tabs = get_field('selected_tab');
$image = $hero['hero_image'];
$choices = get_field_object('selected_tab')['choices'];
//echo '<pre>';
//var_dump($tabs);
//echo '</pre>';
?>

<section id="sHero">
   <div class="heroContainer">
      <div class="heroImageContainer imageOverlay">
         <span><img alt="" src="<?php echo $image  ?>" /></span>
      </div>
      <div class="secondaryHeroDescription">
         <span>
            <h1><?php echo $hero['main_title']; ?></h1>
         </span>
      </div>
   </div>
</section>

<div class="nav-mobile">
   <ul>
      <li>
         <p class="nav-mobile-title" id="sPageTitle">Maria Regina Residence | <a href="<?php bloginfo('url')?>/<?php echo preg_replace('/\W+/', '-', strtolower($tabs));?>/"><?php echo $tabs; ?></a>
         </p>
         <ul class="mobile-menu" style="display: block;">
            <li style="position: relative;">
               <ul id="submenu" style="display:none;">
               <?php foreach( $choices as $choice):?>
                <?php if($choice != $tabs): ?>
                  <li><a href="<?php bloginfo('url')?>/<?php echo preg_replace('/\W+/', '-', strtolower($choice));?>/"><?php echo $choice; ?></a></li>
                  <?php endif; ?>
                <?php endforeach; ?>
               </ul>
            </li>
         </ul>
      </li>
   </ul>
</div>

<div class="container contentSection" id="secondaryContainer">
    <div class="tab-nav">
        <ul>
            <li <?php if (($tabs) == 'Contact') : ?> class="active" <?php endif;  ?>>
                <a href="<?php bloginfo('url')?>/contact/">Contact</a>
            </li>
            <li <?php if (($tabs) == 'Giving') : ?> class="active" <?php endif;  ?>>
                <a href="<?php bloginfo('url')?>/giving/">Giving</a>
            </li>
            <li <?php if (($tabs) == 'Pray for Us') : ?> class="active" <?php endif;  ?>>
                <a href="<?php bloginfo('url')?>/pray-for-us/">Pray for Us</a>
            </li>
        </ul>
    </div>

    <div class="col-md-12">
        <div class="sHeading">
            <div id="secondaryPage-Header">
            <?php if (have_posts()) : while (have_posts()) : the_post();
                the_content();
            endwhile;
        else : ?>
            <p>Sorry, no posts matched your criteria.</p>
        <?php endif; ?>
            </div>
        </div>
    </div>

 
 
     </div>
</div>

<?php get_footer(); ?>