function toggleNavIcon(x) {
    $(x).find("i").toggleClass("fa-bars fa-times");
    $(".respNav").toggleClass("navNotActive");
    $(".hamburgerContainer").toggleClass("navActive");
}

/* $(function() {
    $('a.lightbox').magnificPopup({
        type: 'image',
        closeOnBgClick:false,
        gallery:{
            enabled:true
        }
    });
}); */

$(document).ready(function () {
    if ((window.location.href.indexOf("?edit=1") > -1) || (window.location.href.indexOf("?Edit=1") > -1)) {
        $('.respSecondaryImage').css({ "display": "block" });
        $('.secondaryLocationImage').css({ "max-width": "none" });
        $('#sHero .heroImageContainer').css({ "max-height": "none", "text-align": "center" });
        $('.notRespImage').css({ "height": "auto" });
        $('.carePodText').css({ "z-index": "-1" });
        $('.secondaryHeroDescription').css({ "position": "static", "background-color": "#035f7b" });
        $('.textSize').css({ "display": "none" });
    }

    if (window.location.href.indexOf("/blog/") > -1) {
        $('#nav li a[href *="/blog/"]').addClass("active");
    }

    $(".item:first-of-type").addClass("active");
    $("#nav li a.active").parent('li').addClass("activePage");
    $("#nav li a.active").parents('li').addClass("active");
    $(".mobile-menu li a.active").addClass("currentPage");

    var newURL = $('.currentPage').attr('href');
    $("#sPageTitle").attr('href', newURL);
    $(".sButtonActive").attr('href', newURL);

    $(".mobile-menu li a.active").css({ "display": "none" });
    $(".mobile-menu li a.active").parents('li').addClass("selectedPage");
    $(".buttonNav li a.active").parents('li').addClass("selectedPage");
    $(".buttonNav li a.active").parent('li').addClass("currentParent");
    $(".buttonNav > .selectedPage").siblings("li").addClass("noDisplay");
    $(".mobile-menu > .selectedPage").siblings("li").addClass("noDisplay");
    $(".buttonNav > .selectedPage").siblings("li").css({ "display": "none" });
    $(".mobile-menu > .selectedPage").siblings("li").css({ "display": "none" });
    $(".buttonNav > .selectedPage > a").css({ "display": "none" });
    //$(".mobile-menu .selectedPage").children("a.active").parent(".selectedPage").css({ "display": "none" });
    $(".buttonNav > li.currentParent").css({ "display": "list-item" });

    $(".buttonNav .selectedPage").children('a').siblings("ul").addClass("sButtonList");
    $("#nav li:has(ul)").addClass("hasChild");
    $(".list li ul li:has(ul)").addClass("hasChild");
    $("#nav .hasChild").children('a').append('<span class="respNavArrow mainArrow"><i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></span>');
    $(".hasChild").children('ul:not(.mobile-menu)').css({ "display": "none" });

    $("#nav li a.active").parents('li').addClass("selectedParent");
    $(".selectedParent > a").addClass("active");
    $(".selectedParent > a").children('span').children('i').toggleClass("fa-caret-right fa-caret-down");
    $(".selectedParent > ul").addClass("active-accordion");
    $(".selectedParent > ul").css({ "display": "block" });

    $(".hasChild").css({ "position": "relative" });

    $(".sub-menu").hide();
    $(".current_page_item .sub-menu").show();
	$(".current_page_parent .sub-menu").show();
    $(".menu-item-has-children").children('a').append('<span class="respNavArrow mainArrow"><i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></span>');



    $("span.mainArrow").click(function (e) {
        e.preventDefault();
        var item = $(this).parent().next('ul');
        $(item).addClass("active-accordion");
        if ($(item).is(":visible") && $(item).hasClass("active-accordion")) {
            $(item).slideUp(250);
            $(item).removeClass("active-accordion");
            $(item).prev('a').children(this).children('i').toggleClass("fa-caret-right fa-caret-down");
        }
        else {
            $(item).slideDown(250);
            $(item).addClass("active-accordion");
            $(item).prev('a').children(this).children('i').toggleClass("fa-caret-right fa-caret-down");
        }
    });

    var text = $(".buttonNav > .selectedPage").children("a").text();
    var href = $(".buttonNav > .selectedPage").children("a").attr('href');

    $(".sButtonList").prepend('<li><a></a></li>');
    $(".buttonNav > .currentParent .sButtonList li:first-of-type").addClass("currentParent");

    $(".sButtonList li.currentParent").append('<span class="glyphicon glyphicon-triangle-bottom"></span>');
    $(".sButtonList li.currentParent").addClass("sButtonActive");

    $(".sButtonList li:first-of-type").children("a").text(text);
    $(".sButtonList li:first-of-type").children("a").attr('href', href);

    $('table[id *="_dlEmployees"]').css({ "width": "100%" });
    $('table[id *="_dlEmployees"] tr').addClass("col-xs-6 col-md-4");
    $('table[id *="_dlEmployees"] tr td').css({ "display": "block", "width": "100%" });

    var count = $("ul.sButtonList li:not(.noDisplay) a:not(.noDisplay)").length;
    $('ul.sButtonList li').css({ 'width': 'calc(100% / ' + count + ')' });


    $('.list > li').click(function () {
        $(this).parent().find('ul').fadeToggle("slow");
    });

    $(".buttonNav li a.active").parents(".hideMe").removeClass("hideMe");

    var pageTitle = $.trim($(".sPageTitle").text());
    var listSize = $('.mobile-menu li').size();
    $('.mobile-menu li').each(function () {
        if ($(this).text() == pageTitle) {
            $(this).remove();
        }
    });

    var catcher = $('#stickyCatch');
    var sticky = $('.textSize');

    var curSize;
    $('#increase').click(function () {
        curSize = parseFloat($('body').css('font-size')) + .5;
        if (curSize <= 12.5)
            $('body').css('font-size', (curSize * 10) + "%" );
    });

    $('#decrease').click(function () {
        curSize = parseFloat($('body').css('font-size')) - .5;
        if (curSize >= 10)
            $('body').css('font-size', (curSize * 10) + "%");
    });

    function isScrolledTo(elem) {
        if (elem.length)
        {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();
            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();
            return ((elemTop <= docViewTop));
        }
        return false;
    }

    if (sticky.length && catcher.length)
    {
        if (isScrolledTo(sticky)) {
            sticky.addClass("nowSticky");
        }
        var stopHeight = catcher.offset().top + catcher.height();
        if (stopHeight > sticky.offset().top) {
            sticky.removeClass("nowSticky");
        }
    }

    var podHeight = parseFloat($('.carousel-inner').css('height'));
    $('.newsPod').css('height', podHeight);

    if ($(window).width() > 991) {
        $("#newsCarousel").removeClass("carousel slide");
    }

    if ($(window).width() < 992) {
        $("#newsCarousel").addClass("carousel slide");
    }

    $(window).on('resize', function () {
        if ($(window).width() > 991) {
            $("#newsCarousel").removeClass("carousel slide");
        }

        if ($(window).width() < 992) {
            $("#newsCarousel").addClass("carousel slide");
        }
    });

    $(window).scroll(function () {
        if (isScrolledTo(sticky)) {
            sticky.addClass("nowSticky");
        }
        var stopHeight = catcher.offset().top + catcher.height();
        if (stopHeight > sticky.offset().top) {
            sticky.removeClass("nowSticky");
        }
    });

    var carousel = $("#newsCarousel");
    carousel.append("<ol class='carousel-indicators'></ol>");
    var indicators = $(".carousel-indicators");
    carousel.find(".carousel-inner").children(".item").each(function (index) {
        (index === 0) ?
        indicators.append("<li data-target='#newsCarousel' data-slide-to='" + index + "' class='active'></li>") :
        indicators.append("<li data-target='#newsCarousel' data-slide-to='" + index + "'></li>");
    });

    $(".sButtonList li:not(:first-of-type)").each(function () {
        //var href = $(this).children("a").attr("href");
        //$(this).children("a").attr("href", href + "#secondaryContainer");
        document.getElementById('secondaryContainer').scrollIntoView(true);
    });


    $('#sPageTitle').click(function(){
        console.log('csas');
        $('#submenu').fadeToggle('slow');
        $('#sPageTitle').toggleClass("pressed");
    });

	$(".current_page_parent").find('i').first().removeClass("fa-caret-right").addClass("fa-caret-down");
});


