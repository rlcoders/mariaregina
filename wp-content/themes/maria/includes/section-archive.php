<?php if(have_posts()): while( have_posts()): the_post();?>



<div id="right_content">
                  <div class="PostContent">

                  <div id="CPHMain_ContentPlaceHolder1_dvBacktoList" class="crum">
        <a href="/blog">Blog Home</a> <span class="sep"> &gt;</span> <?php the_title();?>
    </div>
                     <?php if(has_post_thumbnail()):?>
                        <img src="<?php the_post_thumbnail_url();?>">
                     <?php endif;?>
                     <div class="post_header">
                        <div class="left">
                           <div class="post_date">
                              <span><?php echo get_the_date('M d yy' );?></span>
                           </div>
                           <div class="post_title">
                              <span><?php the_title();?></span>
                           </div>
                        </div>
                        <div class="right">
                           <div class="authorImage">
                           </div>
                           <div class="auth_info">
                              <div class="sectiontitle">AUTHOR</div>
                              <span><a rel="nofollow" href="<?php bloginfo('url')?>/author/<?php echo get_the_author_nickname();?>"> <?php the_author();?> » </a></span>
   
                          </div>
                        </div>
                        <div style="clear: both;"></div>
                     </div>
                     <div class="post-content">
                     <?php the_content();?>
                     </div>
                     <div class="post_footer">
                        <div class="left">
                           <div class="sectiontitle">TAGS:</div>

                           <?php $tags = get_the_tags();?>
                        
                           <?php if($tags):?>
                            <?php foreach($tags as $tag):?>
                           <a class="post_tags" rel="nofollow" href="<?php echo get_tag_link($tag->term_id);?>">
                            <?php echo $tag->name;?>
                           </a>
                           <?php endforeach?>
                           <?php endif;?>
                        
                        </div>
                        <div class="right">
                           <div class="sectiontitle">SHARE:</div>
                           <div><?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?></div>
                        </div>
                        <div style="clear: both;"></div>
                     </div>
                  </div>
                  <?php comments_template();?>
                              
               </div>


<?php endwhile; else: endif;?>




