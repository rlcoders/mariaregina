<div class="row">
<div class="col-md-12 contact">
 
 <ul class="nav nav-tabs nav-justified">
   <li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-envelope" aria-hidden="true"></i> Email</a></li>
   <li><a data-toggle="tab" href="#tab2"><i class="fa fa-phone" aria-hidden="true"></i> Call</a></li>
   <li><a data-toggle="tab" href="#tab3"><i class="fa fa-star" aria-hidden="true"></i> Admission</a></li>
 </ul>
 
 <div class="tab-content">
   <div id="tab1" class="tab-pane fade in active">
   <div class="mt-3">
   <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
   </div>

   </div>
   <div id="tab2" class="tab-pane fade text-center">
            <div>
                <b><?php echo get_field('call_info_tab_info')['entrance_text']; ?></b>
            </div>
            <div class="contact-no">
                <p><?php echo get_field('call_info_tab_info')['front_desk_header']; ?></p>
                <a href="tel:<?php echo get_field('header_contact', 'option')['contact_no'];?>"><?php echo get_field('header_contact', 'option')['contact_no'];?></a>
            </div>
            <div class="contact-no">
                <p><?php echo get_field('call_info_tab_info')['admission_header']; ?></p>
                <a href="tel:<?php echo get_field('header_contact', 'option')['contact_no'];?>"><?php echo get_field('header_contact', 'option')['admission_number'];?></a>
            </div>
   </div>
   <div id="tab3" class="tab-pane fade">
      <?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
   </div>
 </div>
</div>