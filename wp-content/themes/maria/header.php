<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="format-detection" content="telephone=no" />
      <title>
         <?php echo get_bloginfo( 'name' ); ?> | <?php echo get_the_title( get_the_ID() );?>
      </title>
      <!-- Favicon -->
      <link rel="shortcut icon" type="image/x-icon"  href="<?php the_field('favicon','option');?>">
      <link rel="manifest" href="<?php bloginfo('template_directory')?>/images/manifest.json">
      <link rel="mask-icon" href="<?php bloginfo('template_directory')?>/images/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="theme-color" content="#ffffff">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
      <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
      <link href="https://fonts.googleapis.com/css?family=La+Belle+Aurore" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
      <?php wp_head();?>
      <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         ga('create', 'UA-91267703-14', 'auto');
         ga('send', 'pageview');
      </script>

	  <!-- Facebook Pixel Code -->
	<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '{1097235036961784}');
  fbq('track', 'PageView');
	</script>
	<noscript>
  <img height="1" width="1" style="display:none"
       src="https://www.facebook.com/tr?id=1097235036961784&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->


      <meta name="Description" content="We strive to bring Christ&#39;s healing comfort and power to all through quality care and a loving home." />
   </head>
   <body id="body1">
      <?php $notificationBar= get_field('header_notification_text','option');?>
	  <?php if( !empty($notificationBar) ): ?>
      <div class="notification-bar" role="region" aria-label="information alert" style="background-color:<?php the_field('header_notification_color','option'); ?>">
         <div class="container">
            <div class="notification-content">
               <h6><?php the_field('header_notification_text','option');?></h6>
               <?php
                  $link = get_field('notification_link','option');
                  if( $link ): ?>
               <a href="<?php the_field('notification_link','option');?>" class="local"><?php the_field('notification_link_text','option');?></a>
               <?php endif; ?>
            </div>
         </div>
         <?php endif; ?>
      </div>
      <div></div>
      <div></div>
      <div id="wrapper">
      <header>
         <div class="container headerContainer noPadding">
            <div class="headerPhoneContainer">
               <div class="headerLogoContainer">
                  <a href="<?php bloginfo('url')?>/"><img alt="Maria Regina" src="<?php the_field('logo','option');?>"></a>
               </div>


               <div class="navbarContainer">
                  <div class="container noPadding">
                     <div class="respNav navNotActive">
                        <?php
                           wp_nav_menu(
                            array(
                              'theme_location' => 'top-menu',
                              'menu' => 'nav',
                              'container' => 'ul',
                              'menu_class' => 'headerNav'
                            )
                           );
                           ?>
                        <a href="https://secure.etransfer.com/EFT/custom/CHI/BlockCodeCommon/donation1.cfm?d2org=MariaRegina&d2tool=Donation" target="_blank" class="donateButton">Donate</a>
                        <a id="tel" href="tel:+16312993000">
                           <p class="respPhoneNumber"><span class="respPhoneStyling"><img src="<?php bloginfo('template_directory')?>/images/phoneWhite.png" /></span><em>Call Us Today</em> (631) 299-3000</p>
                        </a>
                     </div>
                  </div>
               </div>


               <div class="hamburgerContainer">
                  <a class="respNavButton" onclick="toggleNavIcon(this)"><i class="fa fa-bars" aria-hidden="true"></i></a>
               </div>

            </div>
         </div>

      </header>
      <div id="stickyCatch">
      </div>
      <div class="textSize">
         <p>Text Size</p>
         <a id="increase"><img alt="Increase Text Size" src="<?php bloginfo('template_directory')?>/images/increase.png" /></a>
         <a id="decrease"><img alt="Decrease Text Size" src="<?php bloginfo('template_directory')?>/images/decrease.png" /></a>
      </div>
